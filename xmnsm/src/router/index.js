import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
/**
 * 处理相同跳转的错误问题
 */
const routerPush = Router.prototype.push
Router.prototype.push = function push (location) {
  return routerPush.call(this, location).catch(error => error)
}

var setRouter = {
  routes: [
    {
      path: '/',
      name: 'index',
      component: () => import('@/pages/member/login'),
      meta: {
        title: '首页'
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/pages/member/login'),
      meta: {
        title: '登录'
      }
    },
    {
      path: '/mycenter',
      name: 'mycenter',
      component: () => import('@/pages/member/mycenter'),
      meta: {
        isLogin: true
      }
    }
  ]
}

const router = new Router(setRouter)
export default router
