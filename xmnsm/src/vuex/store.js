import Vue from 'vue'
import Vuex from 'vuex'
// 永久存储数据
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const state = {
  userInfo: {}
}

const mutations = {
  setUserInfo (state, userInfo) {
    state.userInfo = userInfo
    console.log(state.userInfo)
  }
}

const store = new Vuex.Store({
  // 永久储存配置
  plugins: [createPersistedState({
    storage: window.sessionStorage,
    reducer (val) {
      return {
        // 只储存state中的某一项
        userInfo: val.userInfo
      }
    }
  })],
  state,
  mutations
})

export default store
